const fs = require('fs');
const path = require('path');
const { exit } = require('process');
const qrcode = require('qrcode-terminal');
const { Client } = require('whatsapp-web.js');
let client;
const SESSION_FILE_PATH = path.join(__dirname, '../session.json');

//Load the session data if it has been previously saved
if (fs.existsSync(SESSION_FILE_PATH)) {
	console.log('* restaurando a sessão * \n');
	const session = require(SESSION_FILE_PATH);
	client = new Client({
		session: session
	});
} else {
	client = new Client();
	client.on('qr', qr => {
		qrcode.generate(qr, { small: true });
	});
}

client.on('ready', () => {
	console.log('Client is ready!');
});

client.on('message', message => {
	console.log(message.body);
});

client.on('authenticated', (session) => {
	sessionData = session;
	fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
		if (err) {
			console.error(err);
		}
	});
});


client.on('message', message => {
	console.log(message.body);
	const LOG = path.join(__dirname, '../mensagens.json')
	console.log(LOG);

	// fs.writeFile(path.join(LOG, JSON.stringify('oi: oi'))), function (err) {
	// 	if (err) {
	// 		console.error(err);
	// 	}
	// }
	fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
				if (err) {
					console.error(err);
				}
			});
	
});

client.initialize();
